const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  UserId: {
    type: Number,
    required: true
  },
  MaterialId: {
    type: Number,
    required: true
  },
  Description: {
    type: String,
    required: true
  },
  ImageUrl: {
    type: String,
    required: true
  },
  Quantity: {
    type: Number,
    required: true
  },
  UOM: {
    type: String,
    required: true
  },
  CostPrice: {
    type: Number,
    required: true
  }
});
const User = mongoose.model('User', userSchema);

module.exports = User;

































