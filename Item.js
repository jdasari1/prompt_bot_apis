const mongoose = require("mongoose");


const materialSchema = new mongoose.Schema({
MaterialNum: { type: String, required: true },
Description: { type: String, required: true },
ImageUrl: { type: String },
UOM: { type: String },
UPC: { type: String },
InventoryQuantity: { type: Number },
CostPrice: { type: Number },
RetailPrice: { type: Number }
});


const Items = mongoose.model("Itemdata", materialSchema);

module.exports = Items;
