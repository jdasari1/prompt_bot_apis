const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  OrderId: {
    type: String,
    required: true
  },
  UserId: {
    type: Number,
    required: true
  },
  MaterialId: {
    type: Number,
    required: true
  },
  
  Quantity: {
    type: Number,
    required: true
  },
  CostPrice: {
    type: Number,
    required: true
  }
});
const Order = mongoose.model('Order', userSchema);

module.exports = Order;



































