const mongoose = require("mongoose");


const materialSchema = new mongoose.Schema({
MaterialId: { type: Number, required: true },
Description: { type: String, required: true },
ImageUrl: { type: String },
UOM: { type: String },
UPC: { type: String },
InventoryQuantity: { type: Number },
CostPrice: { type: Number },
RetailPrice: { type: Number }
});


const Item = mongoose.model("Item", materialSchema);

module.exports = Item;
