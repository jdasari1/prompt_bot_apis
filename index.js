const express = require("express");
const mongoose = require("mongoose");
const Employee = require("./User.js");
const cors = require("cors");
const User = require("./model");
const { v4: uuidv4 } = require("uuid");
const Order = require("./Order");
const Item = require("./Item");
const Items = require("./Items");
const app = express();
require("dotenv").config();
mongoose.connect(
  process.env.mongoURI,
  { useNewUrlParser: true, useUnifiedTopology: true },
  () => console.log("Connection With MongoDB established")
);

app.use(express.json());
app.use(cors());


app.get("/getUserCart", async (req, res) => {
    try {
      const allData = await User.find();
      let TotalCost = 0;
      allData.forEach(item => {
        TotalCost += item.CostPrice * item.Quantity;
      });
      return res.json({ cartItems: allData, TotalCost });
    } catch (err) {
      console.log(err.message);
    }
  });
  
  app.post("/insertnewitem", async (req, res) => {
    const {
      userId,
      materialId,
      description,
      imageUrl,
      quantity,
      uom,
      costPrice
    } = req.body;
    try {
      let albumdata = await User.findOne({
        UserId: userId,
        MaterialId: materialId
      });
      if (albumdata) {
        albumdata.Quantity += quantity;
      } else {
        albumdata = new User({
          UserId: userId,
          MaterialId: materialId,
          Description: description,
          ImageUrl: imageUrl,
          Quantity: quantity,
          UOM: uom,
          CostPrice: costPrice
        });
      }
      await albumdata.save();
      res.json({
        success: true,
        message: "cart table data updated sucessfully.",
        albumdata: albumdata
      });
    } catch (err) {
      res.send({
        success: false,
        message: "Something went wrong",
        error: err
      });
    }
  });
  
  app.get("/cartdata/:userId", async (req, res) => {
    const userId = req.params.userId;
    try {
      const user = await User.find({ UserId: userId });
  
      if (!user || user.length === 0) {
        return res.status(404).json({ message: "User not found" });
      }
      let TotalCost = 0;
      user.forEach(item => {
        TotalCost += item.CostPrice * item.Quantity;
      });
  
      return res.json({ cartItems: user, TotalCost });
    } catch (err) {
      console.log(err.message);
      return res.status(500).json({ message: "Internal server error" });
    }
  });
  
  app.get("/getUserItems", async (req, res) => {
    try {
      const query = req.query.search;
      let userDetails;
      if (isNaN(Number(query))) {
        userDetails = await Item.find(
          {
            Description: { $regex: query, $options: "i" }
          },
          { _id: 0 }
        );
      } else {
        userDetails = await Item.aggregate([
          {
            $addFields: { MaterialIdStr: { $toString: "$MaterialNum" } }
          },
  
          {
            $match: {
              MaterialIdStr: { $regex: query, $options: "i" }
            }
          },
          {
            $project: {
              _id: 0
            }
          }
        ]);
      }
  
      return res.status(200).json({ userDetails: userDetails });
    } catch (err) {
      console.error(err);
      return res.status(500).json({ message: "Internal server error" });
    }
  });
  
  app.post("/placeOrder/:userId", async (req, res) => {
    try {
      const userId = req.params.userId;
      const user = await User.find({ UserId: userId });
      if (!user || user.length === 0) {
        return res.status(404).json({ message: "User not found" });
      }
      let localResults = [];
      var aggregateQuantity = 0;
      var aggregateCost = 0;
      var count;
      const unqId = uuidv4();
      const promises = user.map(async userData => {
        const newOrder = {
          OrderId: unqId,
          UserId: userData.UserId,
          MaterialId: userData.MaterialId,
          CostPrice: userData.CostPrice,
          Quantity: userData.Quantity
        };
        await Order.create(newOrder).then(result => {
          localResults.push(result);
        });
        await User.deleteOne({ _id: userData._id }).then(result => {});
      });
      await Promise.all(promises).then(() => {
        count = localResults.length;
        for (i in localResults) {
          aggregateQuantity = localResults[i].Quantity + aggregateQuantity;
          aggregateCost =
            localResults[i].Quantity * localResults[i].CostPrice + aggregateCost;
        }
      });
      return res.status(200).json({
        OrderId: unqId,
        totalLineItems: count,
        totalItems: aggregateQuantity || 0,
        totalCostPrice: aggregateCost || 0
      });
    } catch (err) {
      console.log(err.message);
      return res.status(500).json({ message: "Internal server error" });
    }
  });
  
  app.post("/updateCart/:userID", async (req, res) => {
    const userID = req.params.userID;
    const cartItems = req.body;
    try {
      const user = await User.findOne({ UserId: userID });
      if (!user) {
        return res.status(404).json({ message: "User not found" });
      }
      cartItems.forEach(async cartItem => {
        const { materialId, quantity } = cartItem;
        if (quantity === 0) {
          await User.findOneAndRemove({ MaterialId: materialId });
        } else {
          await User.findOneAndUpdate(
            { MaterialId: materialId },
            { Quantity: quantity }
          );
        }
      });
      return res.status(200).json({ message: "Success" });
    } catch (error) {
      console.error("Error:", error);
      return res.status(500).json({ message: "Internal server error" });
    }
  });
  
  app.delete("/deleteCartItem/:userID", async (req, res) => {
    const userID = req.params.userID;
    const materialID = req.body.materialId;
    console.log("----------", userID, materialID);
    try {
      const response = await User.findOneAndDelete({
        UserId: userID,
        MaterialId: materialID
      });
      if (!response) {
        return res.status(404).json({ message: "Cart item not found" });
      }
      return res.status(200).json({ message: "Cart item deleted successfully" });
    } catch (error) {
      console.error("Error:", error);
      return res.status(500).json({ message: "Internal server error" });
    }
  });
app.post("/update_password", async (req, res) => {
  let UserId = req.body.UserId;
  let Mail_Id = req.body.Mail_Id;
  const Password = req.body.Password;
  console.log("*******", UserId, Mail_Id, Password);
  try {
    if (!UserId && !Mail_Id) {
      return res
        .status(400)
        .json({ message: "Please provide either UserId or Mail_Id." });
    }
    const user = await Employee.findOne({
      $or: [{ UserId: UserId }, { Mail_Id: Mail_Id }]
    });
    console.log("user***", user);
    if (user) {
      user.Password = Password;
      console.log("user***", user);
      await Employee.replaceOne({ _id: user._id }, user);
      res.status(200).json({
        message: "Password updated successfully.",
        Employee: user
      });
    } else {
      res.status(404).json({ message: "User not found." });
    }
  } catch (error) {
    res.status(500).json({ message: "Internal server error." });
  }
});

app.get("/getUserDetails", async (req, res) => {
  let UserId = req.body.UserId;
  let Mail_Id = req.body.Mail_Id;
  console.log("UserId", UserId, Mail_Id);
  try {
    const user = await Employee.findOne({
      $or: [{ UserId: UserId }, { Mail_Id: Mail_Id }]
    });
    if (!UserId && !Mail_Id) {
      return res
        .status(400)
        .json({ message: "Please provide either UserId or Mail_Id." });
    }

    return res.json({ message: user });
  } catch (err) {
    console.log(err.message);
    return res.status(500).json({ message: "Internal server error" });
  }
});

app.listen(5000, () => {
  console.log("server running");
});
